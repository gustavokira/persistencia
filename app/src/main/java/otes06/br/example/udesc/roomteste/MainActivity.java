package otes06.br.example.udesc.roomteste;

import android.os.AsyncTask;
import android.os.Bundle;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new PopulateTask().execute();
    }

    class PopulateTask extends AsyncTask<Integer, Integer, List<User>>{

        @Override
        protected List<User> doInBackground(Integer... integers) {

            AppDatabase db = Repository.getInstance(getApplicationContext());
            List<User> users = db.userDao().loadAll();
            return users;
        }

        @Override
        protected void onPostExecute( List<User> users) {

          System.out.println(users);
        }
    }
}
